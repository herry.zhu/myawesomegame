const express = require("express");
const app = express();
const loginRouter = require('./routers/router');

app.use("/game", express.static("public"));
app.use(express.json())
app.use('/login', loginRouter);
app.use((req, res, next) => {
    res.status(404).sendFile(__dirname + "/public/404.html");
});

const PORT = 8000;
app.listen(PORT, () => {
    console.log("Server running at port: "+PORT);
});