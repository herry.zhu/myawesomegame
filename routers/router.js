const express = require("express");
const router = express.Router();
const userData = require('../db/users.json')

let isAuth = false;                             
router.post('/', (req,res) => {
    const { username , password} = req.body;
    const user = userData.find(data => {        
        return data.username === username           
    })                                                 

    if (!user){
        isAuth = false;
        return res.status(404).json({
            message: "user non exist"
        })
    }
    if (user.password!==password){
        isAuth = false;
        return res.status(401).json({
            message: "wrong pwd"
        })
    }

    isAuth = true;
    return res.status(200).json({
        message: "login success"
    })
})

router.get('/', (req,res) => {
    if(isAuth){
        // return res.status(200).send("login success")
        return res.status(200).json({
            message: "login success"
        })
    }
    // return res.status(401).send("login failed")
    return res.status(401).json({
        message: "login failed"
    })
})

module.exports = router;
